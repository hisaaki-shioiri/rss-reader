import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

internal class ConverterTest {

    companion object {
        @JvmStatic
        fun removeNP() = listOf(
                Param("[NewsPicks]", "[]"),
                Param("[NewsPicks, NewsPicks]", "[, ]"),
                Param("newsPicks", "newsPicks"),
                Param("newspicks", "newspicks"),
                Param("Newspicks", "Newspicks"),
                Param("", "")
        )
        
        @JvmStatic
        fun brackets() = listOf(
                Param("", "[]"),
                Param("あいうえお", "[あいうえお]"),
                Param("[]", "[[]]"),
                Param("", "[]")
        )
    }

    @ParameterizedTest
    @MethodSource("removeNP")
    internal fun removeNewsPicks(param: Param) {
        val sut = RemoveNewsPicksConverter()
        assertEquals(param.expected, sut.convert(param.value))
    }
    
    @ParameterizedTest
    @MethodSource("brackets")
    internal fun brackets(param: Param) {
        val sut = SurroundBracketsConverter()
        assertEquals(param.expected, sut.convert(param.value))
    }

    data class Param(val value: String, val expected: String)

}