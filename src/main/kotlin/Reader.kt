import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement
import com.fasterxml.jackson.module.kotlin.KotlinModule
import java.net.URL

internal interface Reader {
    fun read(): List<String>
}

internal class UzabaseRssReader : Reader {

    private val rssUri = "https://tech.uzabase.com/rss"

    override fun read(): List<String> {
        return readBody().channel.items.map { it.title }
    }

    private fun readBody(): Rss {
        val xmlMapper = XmlMapper()
        xmlMapper.registerModule(KotlinModule())
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        return URL(rssUri).openConnection().getInputStream().use {
            xmlMapper.readValue(it, Rss::class.java)
        }
    }

    @JacksonXmlRootElement(localName = "rss")
    private data class Rss(val channel: Channel)

    private data class Channel(
            @JacksonXmlProperty(localName = "item")
            @JacksonXmlElementWrapper(useWrapping = false)
            val items: List<Item>
    )

    private data class Item(
            val title: String
    )

}
