
internal interface Writer {
    fun print(value:String)
}

internal class ConsoleWriter : Writer {
    override fun print(value: String) {
        println(value)
    }
}