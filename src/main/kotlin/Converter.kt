
internal interface Converter {
    fun convert(value: String) : String
}

internal class RemoveNewsPicksConverter: Converter {
    override fun convert(value: String): String {
        return value.replace("NewsPicks", "")
    }
}

internal class SurroundBracketsConverter: Converter {
    override fun convert(value: String): String {
        return "[$value]"
    }

}