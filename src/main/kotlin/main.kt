
private val readers = mapOf<String, Reader>(
        "rss" to UzabaseRssReader()
)

private val converters = mapOf<String, Converter>(
        "removeNewsPics" to RemoveNewsPicksConverter(),
        "brackets" to SurroundBracketsConverter()
)

private val writers = mapOf<String, Writer>(
        "stdout" to ConsoleWriter()
)

fun main(args: Array<String>) {
    if (args.size != 3) {
        println("Usages: java MainKt <reader name> <converter names> <writer names>")
        System.exit(1)
    }

    val reader = readers.get(args[0])!!
    val converters = args[1].split(",")
            .map { it.trim() }
            .map { converters.get(it)!! }
    val writers = args[2].split(",")
            .map { it.trim() }
            .map { writers.get(it)!! }
    
    reader.read()
            .map { value ->
                converters.fold(value) { acc, converter -> 
                    converter.convert(acc)
                }
            }
            .forEach { value ->
                writers.forEach { it.print(value) }
            }
}